package main

import (
	"testing"
	"strings"
	"net/http"
	"io/ioutil"
)

/*
Takes a slice of bytes from the body and unmarshals it
 */
func TestCreateGitStruct(t *testing.T) {
	owner := "apache"
	project := "kafka"
	comm := http.Request{}
	comm.Body = ioutil.NopCloser(strings.NewReader(`{
		"login": "ijuma",
		"id": 24747,
		"avatar_url": "https://avatars2.githubusercontent.com/u/24747?v=4",
		"gravatar_id": "",
		"url": "https://api.github.com/users/ijuma",
		"html_url": "https://github.com/ijuma",
		"followers_url": "https://api.github.com/users/ijuma/followers",
		"following_url": "https://api.github.com/users/ijuma/following{/other_user}",
		"gists_url": "https://api.github.com/users/ijuma/gists{/gist_id}",
		"starred_url": "https://api.github.com/users/ijuma/starred{/owner}{/repo}",
		"subscriptions_url": "https://api.github.com/users/ijuma/subscriptions",
		"organizations_url": "https://api.github.com/users/ijuma/orgs",
		"repos_url": "https://api.github.com/users/ijuma/repos",
		"events_url": "https://api.github.com/users/ijuma/events{/privacy}",
		"received_events_url": "https://api.github.com/users/ijuma/received_events",
		"type": "User",
		"site_admin": false,
		"contributions": 342
  	}`))

  	lang := http.Request{}
  	lang.Body = ioutil.NopCloser(strings.NewReader(`{
		"Java": 11493783,
		"Scala": 5114207,
		"Python": 645924,
		"Shell": 85681,
		"Batchfile": 27518,
		"XSLT": 7116,
		"HTML": 5443
	}`))

	gitObj := GitObject{}
	getCommitData(comm.Body, &gitObj)
	getLanguageData(lang.Body, &gitObj)
	gitObj.Owner = owner
	gitObj.Project = project

	if gitObj.Owner != owner {
		t.Errorf("Owner differs. Expected %v, got %v.", owner, gitObj.Owner)
	}

	if gitObj.Project != project {
		t.Errorf("Project differs. Expected %v, got %v.", project, gitObj.Project)
	}

	if gitObj.Committer != "ijuma" {
		t.Errorf("Committer differs. Expected %v, got %v.", "ijuma", gitObj.Committer)
	}

	if gitObj.Commits != 342 {
		t.Errorf("# of commits differs. Expected %v, got %v.", 342, gitObj.Commits)
	}

	if len(gitObj.Language) != 7 {
		t.Errorf("# of languages wrong. Expected %v, got %v.", 7, len(gitObj.Language))
	}
}

/*
Check URL structure
 */
func TestCheckURL(t *testing.T) {
	url := "/projectinfo/v1/github.com/apache/kafka"
	expectedPart1 := "projectinfo"
	expectedPart2 := "v1"
	expectedPart3 := "github.com"
	expectedPart4 := "apache"
	expectedPart5 := "kafka"
	urlParts := checkURL(url)
	if urlParts[1] != expectedPart1 {
		t.Errorf("Part 1 differs. Expected %v, got %v.", expectedPart1, urlParts[1])
	}
	if urlParts[2] != expectedPart2 {
		t.Errorf("Part 2 differs. Expected %v, got %v.", expectedPart2, urlParts[2])
	}
	if urlParts[3] != expectedPart3 {
		t.Errorf("Part 3 differs. Expected %v, got %v.", expectedPart3, urlParts[3])
	}
	if urlParts[4] != expectedPart4 {
		t.Errorf("Part 4 differs. Expected %v, got %v.", expectedPart4, urlParts[4])
	}
	if urlParts[5] != expectedPart5 {
		t.Errorf("Part 5 differs. Expected %v, got %v.", expectedPart5, urlParts[5])
	}
}
/*
Tests formURL function
 */
 func TestFormURLs(t *testing.T) {
 	owner := "apache"
 	project := "kafka"

 	expected := "https://api.github.com/repos/apache/kafka"

 	newUrl := formURL(owner, project)

 	if newUrl != expected {
 		t.Errorf("URL differs. Expected \n%v, got \n%v", expected, newUrl)
	}
 }