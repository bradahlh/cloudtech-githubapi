package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
)

type GitObject struct {
	Project   string
	Owner     string
	Committer string `json:"login"`
	Commits   int    `json:"contributions"`
	Language  []string
}

func main() {
	router := httprouter.New()
	router.GET("/projectinfo/v1/github.com/:owner/:project", gitHandler)
	http.ListenAndServe(":8080", router)
}

func formURL(owner string, project string) string {
	gitURL := "https://api.github.com/repos/" + owner + "/" + project
	return gitURL
}

func getCommitData(r io.ReadCloser, obj *GitObject) {
	err := json.NewDecoder(r).Decode(obj)
	if err != nil {
		fmt.Println("Error when unmarshaling commit data: %v", err)
	}
}

func getLanguageData(r io.ReadCloser, obj *GitObject) {
	lang := make(map[string]int)
	err := json.NewDecoder(r).Decode(&lang)
	if err != nil {
		fmt.Println("Error when unmarshaling languages: %v", err)
	}
	for key, _ := range lang {
		obj.Language = append(obj.Language, key)
	}
}

func gitHandler(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Checks that URL is correctly structured
	urlParts := checkURL(r.URL.String())
	if len(urlParts) == 0 {
		return
	}
	// Empty instance of GitObject to be filled
	gitObj := GitObject{}
	// Fetches 'owner' and 'project' from URL
	gitObj.Owner = p.ByName("owner")
	gitObj.Project = p.ByName("project")
	// Create API URLs for this project
	apiURL := formURL(gitObj.Owner, gitObj.Project)
	languageURL := apiURL + "/languages"
	committerURL := apiURL + "/contributors"

	// Get top committer info
	req, err := http.NewRequest("GET", committerURL, nil)
	if err != nil {
		fmt.Fprintln(w, "Committer URL error: %v", err)
	}
	getCommitData(req.Body, &gitObj)
	defer req.Body.Close()

	// Get languages
	req, err = http.NewRequest("GET", languageURL, nil)
	if err != nil {
		fmt.Fprintln(w, "Language URL error: %v", err)
	}
	getLanguageData(req.Body, &gitObj)

	// Marshal to JSON
	jsonObj, err := json.Marshal(gitObj)
	if err != nil {
		fmt.Fprintln(w, "Error marshaling object: %v", err)
	}
	w.Header().Add("content-type", "application/json")
	w.Write(jsonObj)
}

func checkURL(url string) []string {
	urlParts := strings.Split(url, "/")
	if urlParts[1] != "projectinfo" && urlParts[2] != "v1" && urlParts[3] != "github.com" {
		fmt.Println("URL structure wrong.")
		return []string{}
	}
	return urlParts
}
